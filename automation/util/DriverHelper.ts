import {ElementFinder,element,by,browser,protractor} from "protractor";
import { homePage } from "../pages/homePage";
import {login} from "../pages/login";
var json=require('../testdata/prop.json');

export class DriverHelper {
    
    
    unameTextBox:ElementFinder;
    passTextBox:ElementFinder;
    loginButton:ElementFinder;
        
        
       
          
           clearAndFill (element, value) :void{
              element.clear()
              element.sendKeys(value)
          
      };
      
      
      selectValueFromDropDown(optionVal):void{
          
          element.all(by.tagName("option")).each(function(item)
                     {
                      item.getText().then(function(value)
                              {
                            if(value==optionVal)
                                {
                                item.click()
                                
                                }
                              })
                     })   
   }
      waitForElementToAppear(element){
      var until = protractor.ExpectedConditions;
      browser.wait(until.presenceOf(element), 7000, 'Element taking too long to appear in the DOM');  
      } 
        waitForPageToLoad():void
        {
            browser.waitForAngular()
        }
        
        verifyPageHeader(element):String {
            let headerName="";
      var EC = protractor.ExpectedConditions;
             browser.wait(EC.visibilityOf(element), 17000);
             element.isPresent().then((present) => {
                 if(present) {
                     element.getText().then((msg) => {
                      let header =msg.split('(');
                      let headerName=header[0].toString().trim();
                     
                
                   });
                     
                  }
                 
                });
             console.log("header is from method"+headerName)
             return headerName;
               }    
}