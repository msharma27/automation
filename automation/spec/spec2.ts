import {browser,element,by} from "protractor";
import {login } from "../pages/login";
import {homePage } from "../pages/homePage";
import {userPage } from "../pages/userPage";
import {loadUtils} from "../util/loadUtils";
import {servicePage } from "../pages/servicePage";
var prop1=require('../testdata/applicationProp')

describe("Organisation Test Suite",function()
{
    let lobj=new login();
    let hobj=new homePage();
    let uobj=new userPage;
    let sobj=new servicePage();
    
    //Reading Credentials from JSON File
     let demoUser=prop1.credentials.adminUser;
     let demoPass=prop1.credentials.adminUserPass;
     let adminHeader=prop1.pageHeaders.adminPage;
     
   //Reading Credentials from JSON File
     var bethuneAdmin=prop1.credentials.bethuneAdmin;
     var bethunePass=prop1.credentials.bethunePass;
     
     var fname=prop1.User.fname;
     var lname=prop1.User.lname;
     var serviceType=prop1.User.stype;
     var role=prop1.User.role;
     var service=prop1.User.service;
     var userEmail = "automation"+loadUtils.generateRandomString(4)+"@"+"gmail.com"

  
     beforeEach(() => {
         browser.get(prop1.credentials.url);
         
       });
    
     
     
     it(" Test case 1- Validate user is able to  Create organisation",  function(){
         
       //Reading Credentials from JSON File
         var adminUsename=prop1.credentials.adminUser;
         var adminPassword=prop1.credentials.adminUserPass;
         var adminHeader=prop1.pageHeaders.adminPage;
         var serviceType=prop1.service.type;
         var serviceName = "AutoService"+loadUtils.generateRandomString(4)
         
         //login into application
         lobj.validateLogin(demoUser,demoPass);
         console.log("User has logged in into application  -"+prop1.credentials.demoUser);
         
         /*
          *  Navigate to Administration Page
          */          
            hobj.navigateAdministration();
           console.log("user has navigated to users page");
           
           /*
            *  Create  a service
            */ 
           sobj.createService(serviceName,serviceType)
           console.log("user has created a service");
           
     });

     
});

