import {browser,element,by} from "protractor";
import {login } from "../pages/login";
import {homePage } from "../pages/homePage";
import {userPage } from "../pages/userPage";
import {loadUtils} from "../util/loadUtils";
import {categoryPage} from "../pages/categoryPage";
var prop1=require('../testdata/applicationProp')

describe("Category Creation Test Suite",function()
{
    let lobj=new login();
    let hobj=new homePage();
    let cobj=new categoryPage();
    
    
    //Reading Credentials from JSON File
     let demoUser=prop1.credentials.adminUser;
     let demoPass=prop1.credentials.adminUserPass;
     let adminHeader=prop1.pageHeaders.adminPage;
     var categoryName = "Autocategory"+loadUtils.generateRandomString(4)
  

  
     beforeEach(() => {
         browser.get(prop1.credentials.url);
         
       });
    
     
     
     it(" Test case 1- Validate admin is able to create a  category",  function(){
         
        //login into application
         
         lobj.validateLogin(demoUser,demoPass);
         console.log("User has logged in into application  -"+prop1.credentials.demoUser);
         
         /*
          * navigate to category
          */ 
         hobj.navigateToCategoryPage();
         console.log("User has navigated to category page");
         
         /*
          *  Create  a category
          */ 
         cobj.createCategory(categoryName)
         console.log("user has succesfully created a category");
         
         
         
           
     });
     
     

});

