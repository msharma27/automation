import {browser,element,by} from "protractor";
import {login } from "../pages/login";
import {homePage } from "../pages/homePage";
import {userPage } from "../pages/userPage";
import {loadUtils} from "../util/loadUtils";
var prop1=require('../testdata/applicationProp')

describe("Login Test Suite",function()
{
    let lobj=new login();
    let homepage
    
    
    //Reading Credentials from JSON File
     let demoUser=prop1.credentials.adminUser;
     let demoPass=prop1.credentials.adminUserPass;
     let adminHeader=prop1.pageHeaders.adminPage;
  
    
  
     beforeAll(() => {
         browser.get(prop1.credentials.url);
         
       });
    
     
     
     it(" Test case 1- Validate user is able to login into application",  function(){
         
        //login into application
         homepage= lobj.validateLogin(demoUser,demoPass);
         console.log("User has logged in into application  -"+prop1.credentials.demoUser);
       
         
        
     });
     
     it(" Test case 2- Validate user is able to logout from application",  function(){
         
         //logout from application
         homepage.logout();
         console.log("User has logged out from application");
         
      });
     
     afterAll(() => {
         browser.close()
         
     });
});

