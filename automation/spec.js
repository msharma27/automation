"use strict";
exports.__esModule = true;
var protractor_1 = require("protractor");
describe('angularjs homepage', function () {
    it('should greet the named user', function (done) {
        // Load the AngularJS homepage.
        protractor_1.browser.get('h');
        // Find the element with ng-model matching 'yourName' - this will
        // find the <input type="text" ng-model="yourName"/> element - and then
        // type 'Julie' into it.
        protractor_1.element(protractor_1.by.model('yourName')).sendKeys('Julie');
        // Find the element with binding matching 'yourName' - this will
        // find the <h1>Hello {{yourName}}!</h1> element.
        var greeting = protractor_1.element(protractor_1.by.binding('yourName'));
        // Assert that the text element has the expected value.
        // Protractor patches 'expect' to understand promises.
        expect(greeting.getText()).toEqual('Hello Julie!');
    });
});
