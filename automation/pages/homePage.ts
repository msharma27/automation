import {ElementFinder,element,by,browser} from "protractor";
import { userPage } from "../pages/userPage";
import {DriverHelper} from "../util/DriverHelper";
import { login } from "../pages/login";



 export class homePage extends DriverHelper{
     
   
    
     userTab:ElementFinder=element(by.css("a[href='/fe/admin/users/all']"));
     roleDropDown:ElementFinder=element(by.id("roledropdown"));
     do_Role:ElementFinder= element(by.xpath("//a[text()=' DO']"));
     userPageHeader:ElementFinder=element(by.css("service-id-title.clearfix>h3")); 
     adminTab:ElementFinder=element(by.id('admin'))
     categoryTab:ElementFinder=element(by.xpath("//a[contains(text(),'Categories')]"))
     categoryTitle:ElementFinder=element(by.css("h2[class='title-name']"));
     userProfileDrop:ElementFinder=element(by.css(".truncatr-header.nick"))
     logoutBtn:ElementFinder=element(by.css(".btn.btn-primary.diconnection-btn"))
     
     navigateToUsers():userPage {
        this.userTab.click();
        lobj.waitForPageToLoad()
        
        return new userPage();
     };   
     
     navigateAdministration()  {
         this.adminTab.click();
         lobj.waitForPageToLoad()
         
         
      };  
      
      navigateToCategoryPage()
      {
      this.navigateAdministration();
      this.categoryTab.click();
      lobj.waitForPageToLoad()
      lobj.verifyPageHeader(this.categoryTitle)    
      };
 
      switchRole(role:String){
          this.roleDropDown.click();
         if (role=='DO')
         {
           this.do_Role.click 
         }
         else 
         {
             
         }
      }
          
   logout():login{
       this.userProfileDrop.click()
       this.logoutBtn.click()
       return new login();
   }
      
        
}
 let lobj=new homePage();
