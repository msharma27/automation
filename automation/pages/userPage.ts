import {ElementFinder,element,by,browser} from "protractor";
import {DriverHelper} from "../util/DriverHelper";



 export class userPage extends DriverHelper{
     
      nameTextBox:ElementFinder = element(by.id('email'));
     dynamicText:ElementFinder = element(by.id('pwd'));
     loginButton:ElementFinder = element(by.css("button[type='submit']"));
     adminTab:ElementFinder =    element(by.id('admin'));
     loginError:ElementFinder=   element(by.css("span[text()='Wrong  username. Try again.']"));
     profileDropDown:ElementFinder= element(by.xpath("//a[@class='truncatr-header nick']"));
     logoutBtn:ElementFinder=element(by.css("button.btn.btn-primary.diconnection-btn"))
     roleDropDown:ElementFinder=element(by.id("roledropdown"));
     do:ElementFinder= element(by.xpath("//a[text()=' DO']"));
     addUserBtn:ElementFinder=element(by.xpath("//button[text()='Create']"));
     userFormHeader:ElementFinder=element(by.css("h4.modal-title"))
     firstname:ElementFinder=element(by.id("firstName"));
     lastname:ElementFinder=element(by.id("lastName"));
     email:ElementFinder=element(by.id("email"));
     servicetypeInt:ElementFinder=element(by.id("radio_internal"));
     servicetypePartner:ElementFinder=element(by.id("radio_partner"));
     roledo:ElementFinder=element(by.id("radio_owner_0"));
     next:ElementFinder=element(by.xpath("//button[text()='Next step']"))
     create:ElementFinder=element(by.xpath("//input[@id='radio_admin']//following::button[text()='Create']"))
     getEmail:ElementFinder=element(by.id('mail'))
     senderEmail:ElementFinder=element(by.css("span[class='inboxSenderEmail']"))
     searchField:ElementFinder=element(by.css("input[placeholder=' Search a user']"))
     searchIcon:ElementFinder=element(by.css(".fa.fa-search"))
     successMessageOnUser:ElementFinder=element(by.css("div.col-md-6.col-sm-6>p"))
     okBtn:ElementFinder=element(by.xpath("//button[text()='OK']"))
     
     
     createUser(fname,lname,userEmail,serviceType,serviceName,Role):userPage{
         lobj.clearAndFill(this.firstname,fname)
         lobj.clearAndFill(this.lastname,lname)
         lobj.clearAndFill(this.email,userEmail)
         this.next.click()
         this.servicetypeInt.click()
         lobj.selectValueFromDropDown(serviceName)
         this.roledo.click()
         this.create.click()
         var greeting = this.successMessageOnUser.getText();
         expect(greeting).toEqual("The user was successfully created.");
         this.okBtn.click()
         return new userPage()
         }
        
     ClickOnaddUserBtn():userPage {
      this.addUserBtn.click()
         var greeting = this.userFormHeader.getText();
          expect(greeting).toEqual("Add a user");
          return new userPage;
         
     }
          
   
      
        
}
 let lobj=new userPage();
