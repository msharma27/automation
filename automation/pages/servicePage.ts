import {ElementFinder,element,by,browser} from "protractor";
import { homePage } from "../pages/homePage";
import {DriverHelper} from "../util/DriverHelper";



 export class servicePage extends DriverHelper{
     
   
     serviceTypeInt=element(by.id('radio_INTERNAL'))
     serviceTypePartner=element(by.id('radio_PARTNER'))
      create=element(by.xpath("//button[text()='Create']"))
      serviceName=element(by.id('serviceName'))
      successMessageOnUser=element(by.css("div.col-md-6.col-sm-6>p"))
      save=element(by.xpath("//button[text()='Save']"));
      okBtn=element(by.xpath("//button[text()='OK']"))
      
     createService(serviceName,serviceType){
        this.create.click()
        lobj.clearAndFill(this.serviceName,serviceName)
        lobj.selectValueFromDropDown(serviceType)
        this.save.click()
        this.successMessageOnUser.getText().then(function(actualText)
         {
              
                expect(actualText).toEqual("The organisation was successfully created");
                })
        this.okBtn.click()
     }
          
   
      
        
}
let lobj=new servicePage();

