import {ElementFinder,element,by,browser} from "protractor";
import { homePage } from "../pages/homePage";
import {DriverHelper} from "../util/DriverHelper";
var prop1=require('../testdata/applicationProp')


 export class login extends DriverHelper{
     
   
    unameTextBox:ElementFinder=element(by.id('email'));
    passTextBox:ElementFinder=element(by.id('pwd'))
    loginButton:ElementFinder=element(by.css("button[type='submit']"));
    adminTab:ElementFinder =element(by.id('admin'));
    welcomeBanner:ElementFinder= element(by.xpath("//img[@src='/assets/images/welcome_pic.png']//preceding::div[1]"))
    
    
       
   
        
        
          validateLogin(username:string,password:string):homePage
        {
            lobj.clearAndFill(this.unameTextBox,username)
            lobj.clearAndFill(this.passTextBox,password)
            this.loginButton.click();
            lobj.waitForPageToLoad()
            if((this.welcomeBanner.isDisplayed()&&this.adminTab.isDisplayed())){
                this.welcomeBanner.getText().then(function(banner)
                {
                let welcomeText:String= banner.trim();
                let expected:String=prop1.pageHeaders.welcome.trim();
                expect(welcomeText).toEqual(expected);
                })
            }
            return new homePage();
        }
          
       
      
        
}
 let lobj=new login();
