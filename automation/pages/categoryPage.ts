import {ElementFinder,element,by,browser} from "protractor";
import { userPage } from "../pages/userPage";
import {DriverHelper} from "../util/DriverHelper";



 export class categoryPage extends DriverHelper{
     
   create_Category:ElementFinder=element(by.xpath("//button[text()='Create']"))
   categoryName:ElementFinder=element(by.id('name'))
   saveCategory:ElementFinder=element(by.id('org_save'))
   okBtn1:ElementFinder=element(by.xpath("//button[text()='OK']"))
   successMessageOnCategory:ElementFinder=element(by.css("div.col-md-6.col-sm-6>p"))
    
   createCategory(cName)
   {
       this.create_Category.click()
       lobj.clearAndFill(this.categoryName,cName)
       this.saveCategory.click()
       lobj.verifyPageHeader(this.successMessageOnCategory)  
       
    }
         
    
 }
 
 let lobj=new categoryPage();