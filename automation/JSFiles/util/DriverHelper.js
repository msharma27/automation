"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var json = require('../testdata/prop.json');
var DriverHelper = /** @class */ (function () {
    function DriverHelper() {
    }
    DriverHelper.prototype.clearAndFill = function (element, value) {
        element.clear();
        element.sendKeys(value);
    };
    ;
    DriverHelper.prototype.selectValueFromDropDown = function (optionVal) {
        protractor_1.element.all(protractor_1.by.tagName("option")).each(function (item) {
            item.getText().then(function (value) {
                if (value == optionVal) {
                    item.click();
                }
            });
        });
    };
    DriverHelper.prototype.waitForElementToAppear = function (element) {
        var until = protractor_1.protractor.ExpectedConditions;
        protractor_1.browser.wait(until.presenceOf(element), 7000, 'Element taking too long to appear in the DOM');
    };
    DriverHelper.prototype.waitForPageToLoad = function () {
        protractor_1.browser.waitForAngular();
    };
    DriverHelper.prototype.verifyPageHeader = function (element) {
        var headerName = "";
        var EC = protractor_1.protractor.ExpectedConditions;
        protractor_1.browser.wait(EC.visibilityOf(element), 17000);
        element.isPresent().then(function (present) {
            if (present) {
                element.getText().then(function (msg) {
                    var header = msg.split('(');
                    var headerName = header[0].toString().trim();
                });
            }
        });
        console.log("header is from method" + headerName);
        return headerName;
    };
    return DriverHelper;
}());
exports.DriverHelper = DriverHelper;
