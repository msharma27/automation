"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var HtmlReporter = require('protractor-beautiful-reporter');
exports.config = {
    // your config here ...
    onPrepare: function () {
        protractor_1.browser.manage().window().maximize();
        // Add a screenshot reporter and store screenshots to `/tmp/screenshots`:
        jasmine.getEnv().addReporter(new HtmlReporter({
            baseDirectory: 'tmp/screenshots',
            docName: 'index.html',
            preserveDirectory: false,
            gatherBrowserLogs: false,
            screenshotsSubfolder: 'images',
            jsonsSubfolder: 'jsons',
            takeScreenShotsOnlyForFailedSpecs: true,
            docTitle: 'LDH Automation Suite'
        }).getJasmine2Reporter());
    },
    directConnect: true,
    framework: 'jasmine2',
    capabilities: {
        browserName: 'chrome',
        shardTestFiles: true,
        maxInstances: 1
    },
    specs: ['./spec/spec2.js'],
    /*suites:{
        smoke:'./specs/spec3.js']
        
    },*/
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 2500000
    }
};
