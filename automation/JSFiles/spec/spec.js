"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
describe('angularjs homepage', () => {
    it('should greet the named user', () => __awaiter(this, void 0, void 0, function* () {
        // Load the AngularJS homepage.
        protractor_1.browser.get('http://www.angularjs.org');
        // Find the element with ng-model matching 'yourName' - this will
        // find the <input type="text" ng-model="yourName"/> element - and then
        // type 'Julie' into it.
        yield protractor_1.element(protractor_1.by.model('yourName')).sendKeys('Julie');
        // Find the element with binding matching 'yourName' - this will
        // find the <h1>Hello {{yourName}}!</h1> element.
        var greeting = yield protractor_1.element(protractor_1.by.binding('yourName'));
        // Assert that the text element has the expected value.
        // Protractor patches 'expect' to understand promises.
        yield expect(greeting.getText()).toEqual('Hello Julie!');
    }));
});
