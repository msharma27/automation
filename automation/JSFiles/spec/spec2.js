"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var login_1 = require("../pages/login");
var homePage_1 = require("../pages/homePage");
var userPage_1 = require("../pages/userPage");
var loadUtils_1 = require("../util/loadUtils");
var servicePage_1 = require("../pages/servicePage");
var prop1 = require('../testdata/applicationProp');
describe("Organisation Test Suite", function () {
    var lobj = new login_1.login();
    var hobj = new homePage_1.homePage();
    var uobj = new userPage_1.userPage;
    var sobj = new servicePage_1.servicePage();
    //Reading Credentials from JSON File
    var demoUser = prop1.credentials.adminUser;
    var demoPass = prop1.credentials.adminUserPass;
    var adminHeader = prop1.pageHeaders.adminPage;
    //Reading Credentials from JSON File
    var bethuneAdmin = prop1.credentials.bethuneAdmin;
    var bethunePass = prop1.credentials.bethunePass;
    var fname = prop1.User.fname;
    var lname = prop1.User.lname;
    var serviceType = prop1.User.stype;
    var role = prop1.User.role;
    var service = prop1.User.service;
    var userEmail = "automation" + loadUtils_1.loadUtils.generateRandomString(4) + "@" + "gmail.com";
    beforeEach(function () {
        protractor_1.browser.get(prop1.credentials.url);
    });
    it(" Test case 1- Validate user is able to  Create organisation", function () {
        //Reading Credentials from JSON File
        var adminUsename = prop1.credentials.adminUser;
        var adminPassword = prop1.credentials.adminUserPass;
        var adminHeader = prop1.pageHeaders.adminPage;
        var serviceType = prop1.service.type;
        var serviceName = "AutoService" + loadUtils_1.loadUtils.generateRandomString(4);
        //login into application
        lobj.validateLogin(demoUser, demoPass);
        console.log("User has logged in into application  -" + prop1.credentials.demoUser);
        /*
         *  Navigate to Administration Page
         */
        hobj.navigateAdministration();
        console.log("user has navigated to users page");
        /*
         *  Create  a service
         */
        sobj.createService(serviceName, serviceType);
        console.log("user has created a service");
    });
});
