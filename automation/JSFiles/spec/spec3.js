"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var login_1 = require("../pages/login");
var prop1 = require('../testdata/applicationProp');
describe("Login Test Suite", function () {
    var lobj = new login_1.login();
    var homepage;
    //Reading Credentials from JSON File
    var demoUser = prop1.credentials.adminUser;
    var demoPass = prop1.credentials.adminUserPass;
    var adminHeader = prop1.pageHeaders.adminPage;
    beforeAll(function () {
        protractor_1.browser.get(prop1.credentials.url);
    });
    it(" Test case 1- Validate user is able to login into application", function () {
        //login into application
        homepage = lobj.validateLogin(demoUser, demoPass);
        console.log("User has logged in into application  -" + prop1.credentials.demoUser);
    });
    it(" Test case 2- Validate user is able to logout from application", function () {
        //logout from application
        homepage.logout();
        console.log("User has logged out from application");
    });
    afterAll(function () {
        protractor_1.browser.close();
    });
});
