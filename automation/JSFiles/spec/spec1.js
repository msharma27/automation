"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var login_1 = require("../pages/login");
var homePage_1 = require("../pages/homePage");
var userPage_1 = require("../pages/userPage");
var loadUtils_1 = require("../util/loadUtils");
var prop1 = require('../testdata/applicationProp');
describe("User Creation Test Suite", function () {
    var lobj = new login_1.login();
    var hobj = new homePage_1.homePage();
    var uobj = new userPage_1.userPage;
    //Reading Credentials from JSON File
    var demoUser = prop1.credentials.adminUser;
    var demoPass = prop1.credentials.adminUserPass;
    var adminHeader = prop1.pageHeaders.adminPage;
    //Reading Credentials from JSON File
    var bethuneAdmin = prop1.credentials.bethuneAdmin;
    var bethunePass = prop1.credentials.bethunePass;
    var fname = prop1.User.fname;
    var lname = prop1.User.lname;
    var serviceType = prop1.User.stype;
    var role = prop1.User.role;
    var service = prop1.User.service;
    var userEmail = "automation" + loadUtils_1.loadUtils.generateRandomString(4) + "@" + "gmail.com";
    beforeEach(function () {
        protractor_1.browser.get(prop1.credentials.url);
    });
    it(" Test case 1- Validate user is able to create a  user", function () {
        //login into application
        lobj.validateLogin(demoUser, demoPass);
        console.log("User has logged in into application  -" + prop1.credentials.demoUser);
        /*
         *  Navigate to Administration Page
         */
        hobj.navigateToUsers();
        console.log("user has navigated to users page");
        /*
        *  Click on Add user Button
        */
        uobj.ClickOnaddUserBtn();
        console.log("user has clicked on Add user button");
        /*
         *  Adding a new user using it's first name ,last name
         */
        uobj.createUser(fname, lname, userEmail, serviceType, service, role);
        console.log("user has navigated to users page");
    });
});
