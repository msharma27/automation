"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var login_1 = require("../pages/login");
var homePage_1 = require("../pages/homePage");
var loadUtils_1 = require("../util/loadUtils");
var categoryPage_1 = require("../pages/categoryPage");
var prop1 = require('../testdata/applicationProp');
describe("Category Creation Test Suite", function () {
    var lobj = new login_1.login();
    var hobj = new homePage_1.homePage();
    var cobj = new categoryPage_1.categoryPage();
    //Reading Credentials from JSON File
    var demoUser = prop1.credentials.adminUser;
    var demoPass = prop1.credentials.adminUserPass;
    var adminHeader = prop1.pageHeaders.adminPage;
    var categoryName = "Autocategory" + loadUtils_1.loadUtils.generateRandomString(4);
    beforeEach(function () {
        protractor_1.browser.get(prop1.credentials.url);
    });
    it(" Test case 1- Validate admin is able to create a  category", function () {
        //login into application
        lobj.validateLogin(demoUser, demoPass);
        console.log("User has logged in into application  -" + prop1.credentials.demoUser);
        /*
         * navigate to category
         */
        hobj.navigateToCategoryPage();
        console.log("User has navigated to category page");
        /*
         *  Create  a category
         */
        cobj.createCategory(categoryName);
        console.log("user has succesfully created a category");
    });
});
