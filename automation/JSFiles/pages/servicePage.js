"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var DriverHelper_1 = require("../util/DriverHelper");
var servicePage = /** @class */ (function (_super) {
    __extends(servicePage, _super);
    function servicePage() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.serviceTypeInt = protractor_1.element(protractor_1.by.id('radio_INTERNAL'));
        _this.serviceTypePartner = protractor_1.element(protractor_1.by.id('radio_PARTNER'));
        _this.create = protractor_1.element(protractor_1.by.xpath("//button[text()='Create']"));
        _this.serviceName = protractor_1.element(protractor_1.by.id('serviceName'));
        _this.successMessageOnUser = protractor_1.element(protractor_1.by.css("div.col-md-6.col-sm-6>p"));
        _this.save = protractor_1.element(protractor_1.by.xpath("//button[text()='Save']"));
        _this.okBtn = protractor_1.element(protractor_1.by.xpath("//button[text()='OK']"));
        return _this;
    }
    servicePage.prototype.createService = function (serviceName, serviceType) {
        this.create.click();
        lobj.clearAndFill(this.serviceName, serviceName);
        lobj.selectValueFromDropDown(serviceType);
        this.save.click();
        this.successMessageOnUser.getText().then(function (actualText) {
            expect(actualText).toEqual("The organisation was successfully created");
        });
        this.okBtn.click();
    };
    return servicePage;
}(DriverHelper_1.DriverHelper));
exports.servicePage = servicePage;
var lobj = new servicePage();
