"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var userPage_1 = require("../pages/userPage");
var DriverHelper_1 = require("../util/DriverHelper");
var login_1 = require("../pages/login");
var homePage = /** @class */ (function (_super) {
    __extends(homePage, _super);
    function homePage() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.userTab = protractor_1.element(protractor_1.by.css("a[href='/fe/admin/users/all']"));
        _this.roleDropDown = protractor_1.element(protractor_1.by.id("roledropdown"));
        _this.do_Role = protractor_1.element(protractor_1.by.xpath("//a[text()=' DO']"));
        _this.userPageHeader = protractor_1.element(protractor_1.by.css("service-id-title.clearfix>h3"));
        _this.adminTab = protractor_1.element(protractor_1.by.id('admin'));
        _this.categoryTab = protractor_1.element(protractor_1.by.xpath("//a[contains(text(),'Categories')]"));
        _this.categoryTitle = protractor_1.element(protractor_1.by.css("h2[class='title-name']"));
        _this.userProfileDrop = protractor_1.element(protractor_1.by.css(".truncatr-header.nick"));
        _this.logoutBtn = protractor_1.element(protractor_1.by.css(".btn.btn-primary.diconnection-btn"));
        return _this;
    }
    homePage.prototype.navigateToUsers = function () {
        this.userTab.click();
        lobj.waitForPageToLoad();
        return new userPage_1.userPage();
    };
    ;
    homePage.prototype.navigateAdministration = function () {
        this.adminTab.click();
        lobj.waitForPageToLoad();
    };
    ;
    homePage.prototype.navigateToCategoryPage = function () {
        this.navigateAdministration();
        this.categoryTab.click();
        lobj.waitForPageToLoad();
        lobj.verifyPageHeader(this.categoryTitle);
    };
    ;
    homePage.prototype.switchRole = function (role) {
        this.roleDropDown.click();
        if (role == 'DO') {
            this.do_Role.click;
        }
        else {
        }
    };
    homePage.prototype.logout = function () {
        this.userProfileDrop.click();
        this.logoutBtn.click();
        return new login_1.login();
    };
    return homePage;
}(DriverHelper_1.DriverHelper));
exports.homePage = homePage;
var lobj = new homePage();
