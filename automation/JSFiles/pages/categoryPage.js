"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var DriverHelper_1 = require("../util/DriverHelper");
var categoryPage = /** @class */ (function (_super) {
    __extends(categoryPage, _super);
    function categoryPage() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.create_Category = protractor_1.element(protractor_1.by.xpath("//button[text()='Create']"));
        _this.categoryName = protractor_1.element(protractor_1.by.id('name'));
        _this.saveCategory = protractor_1.element(protractor_1.by.id('org_save'));
        _this.okBtn1 = protractor_1.element(protractor_1.by.xpath("//button[text()='OK']"));
        _this.successMessageOnCategory = protractor_1.element(protractor_1.by.css("div.col-md-6.col-sm-6>p"));
        return _this;
    }
    categoryPage.prototype.createCategory = function (cName) {
        this.create_Category.click();
        lobj.clearAndFill(this.categoryName, cName);
        this.saveCategory.click();
        lobj.verifyPageHeader(this.successMessageOnCategory);
    };
    return categoryPage;
}(DriverHelper_1.DriverHelper));
exports.categoryPage = categoryPage;
var lobj = new categoryPage();
